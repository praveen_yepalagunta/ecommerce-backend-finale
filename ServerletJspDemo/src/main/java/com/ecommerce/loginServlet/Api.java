package com.ecommerce.loginServlet;



import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.ecommerce.*;
import com.ecommerce.login.User;
import com.ecommerce.loginDao.UserDao;
import com.google.gson.Gson;





/**
 * Servlet implementation class Api
 */
public class Api extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Api() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		/*get username and password from frontend, execute query and send valid user message to frontend...*/
		
		response.addHeader("Access-Control-Allow-Origin", "*");
	    response.addHeader("Access-Control-Allow-Methods", "*");
	    response.addHeader("Access-Control-Allow-Headers", "*");
		PrintWriter out = response.getWriter();
		int status = 0;
		String username=request.getParameter("username");
		String password = request.getParameter("password");
			
		UserDao userdao=new UserDao();
		User rec = null;
		 
			try {

				rec = userdao.searchForUser(username, password);
				if (rec != null) {
					status = 1;
				}
							
				
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String productJsonString = new Gson().toJson(rec);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			out.print(productJsonString);
			// System.out.println(productJsonString);
			out.flush();
	        
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,NumberFormatException {
		response.addHeader("Access-Control-Allow-Origin", "*");
	    response.addHeader("Access-Control-Allow-Methods", "*");
	    response.addHeader("Access-Control-Allow-Headers", "*");
		// TODO Auto-generated method stub
		UserDao dao =new UserDao();
		User user=null;
		PrintWriter p =response.getWriter();
//		String address="abdul";
		int user_id=Integer.parseInt(request.getParameter("user_id"));

        String username=request.getParameter("username");
        String password=request.getParameter("password");
        String email_id=request.getParameter("email_id");
        String address=request.getParameter("address");
        String user_role=request.getParameter("user_role");
        System.out.println(address);
       
        user = new User(user_id,username,password,email_id, address, user_role);
        
       //p.print(request.getParameter("prodDesc"));
       //String =request.getParameter("prodDesc");
        //double price=Double.parseDouble(request.getParameter("price"));
        //int qty=Integer.parseInt(request.getParameter("qty"));
        
		int upd=404;
		try {
			int record=dao.registerUser(user);
			
			p.print(record);
			if(record>0)
			{
				//if record inserted 200
				upd=200;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch bloc
			e.printStackTrace();
			//failure status
			upd=404;
		}
		String productJsonString=new Gson().toJson(upd);

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		p.print(productJsonString);
		p.flush();
	
	}

}
