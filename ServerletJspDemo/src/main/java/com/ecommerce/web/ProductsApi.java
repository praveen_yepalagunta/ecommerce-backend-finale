package com.ecommerce.web;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.ecommerce.products.Products;
import com.ecommerce.web.dao.ProductsDao;
import com.google.gson.Gson;


/**
 * Servlet implementation class ProductsApi
 */
@WebServlet("/db")
public class ProductsApi extends HttpServlet {
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		  response.addHeader("Access-Control-Allow-Origin", "*");
		    response.addHeader("Access-Control-Allow-Methods", "*");
		    response.addHeader("Access-Control-Allow-Headers", "http://localhost:3000/");
		    
		    int cate = Integer.parseInt(request.getParameter("category_id"));
//		    int cate=1;
		    
        ProductsDao dao=new ProductsDao();
        List<Products> users=new ArrayList<>();
	    users=dao.getProducts(cate);
		Gson gson = new Gson();
		String userJSON;
		try {
			userJSON = gson.toJson(users);
			PrintWriter pw=response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			pw.write(userJSON);
			pw.close();
		} catch (Exception e) {
			e.printStackTrace();
			}
	}
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.addHeader("Access-Control-Allow-Origin", "*");
	    response.addHeader("Access-Control-Allow-Methods", "*");
	    response.addHeader("Access-Control-Allow-Headers", "http://localhost:3000/");
		ProductsDao dao =new ProductsDao();
		Products prod = null;
		PrintWriter p =response.getWriter();
		int id =Integer.parseInt(request.getParameter("id"));
        String productName=request.getParameter("prodname");
       String prodDesc=request.getParameter("Description");
        double price=Double.parseDouble(request.getParameter("price"));
        int qty=Integer.parseInt(request.getParameter("Quantity"));
        int category_id=Integer.parseInt(request.getParameter("category_id"));
        String image=request.getParameter("image");
       prod = new Products(id,productName, price, qty, prodDesc, category_id, image);
		int upd=404;
	
			int record=0;
			try {
				record = dao.addInventory(prod);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		if(record>0)
		{
			//if record inserted 200
			upd=200;
		}
		else {
		upd=404;
	}
		String productJsonString=new Gson().toJson(upd);

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		p.print(productJsonString);
		p.flush();
	}
 
	/**
	 * @see HttpServlet#doPut(HttpServletRequest, HttpServletResponse)
	 */
	public void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.addHeader("Access-Control-Allow-Origin", "*");
	    response.addHeader("Access-Control-Allow-Methods", "*");
	    response.addHeader("Access-Control-Allow-Headers", "http://localhost:3000/");
	    response.setHeader("Access-Control-Allow-Origin", "*");
		PrintWriter out = response.getWriter();
		ProductsDao dao = new ProductsDao();
		int status = 100;
    	int prod_id = Integer.parseInt(request.getParameter("id"));
        String prod_name = request.getParameter("prodname");
        double prod_price = Double.parseDouble(request.getParameter("price"));
		int qty = Integer.parseInt(request.getParameter("Quantity"));
     	String prod_desc = request.getParameter("Description");
     	String image = request.getParameter("image");
		int rec = 0;
		try {
			rec = dao.updateProductUsingId(prod_id, prod_price,prod_name,qty,prod_desc, image);
			if (rec > 0) {
				status = 200;
			}
			String productJsonString = new Gson().toJson(status);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			out.print(productJsonString);
			out.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doDelete(HttpServletRequest, HttpServletResponse)
	 */

	public void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "*");
		response.addHeader("Access-Control-Allow-Headers", "*");
		ProductsDao dao = new ProductsDao();
		 PrintWriter out = response.getWriter();
		 int prod_id = Integer.parseInt(request.getParameter("prod_id"));
		int status;
		int rec = 0;
		 try {
			 rec = dao.deleteProduct(prod_id);
		 } catch (Exception b) {
		 b.printStackTrace();
		}
		 if (rec > 0) {
		 status=200;
		}else {
		 status = 404;
		}
		 String productJsonString = new Gson().toJson(status);
		 response.setContentType("application/json");
		 response.setCharacterEncoding("UTF-8");
		out.write(productJsonString);
		System.out.println(rec);
		out.close();
 }
			
		
}
